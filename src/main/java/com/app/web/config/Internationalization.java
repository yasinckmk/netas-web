//package com.app.web.config;
//
//import java.util.Locale;
//
//import org.springframework.context.MessageSource;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.support.ResourceBundleMessageSource;
//import org.springframework.web.servlet.LocaleResolver;
//import org.springframework.web.servlet.config.annotation.EnableWebMvc;
//import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
//import org.springframework.web.servlet.i18n.CookieLocaleResolver;
//import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
//import org.springframework.web.servlet.i18n.SessionLocaleResolver;
//
//@Configuration
//@EnableWebMvc
//public class Internationalization extends WebMvcConfigurerAdapter {
//  
//   @Bean
//   public LocaleChangeInterceptor localeChangeInterceptor() {
//      LocaleChangeInterceptor localeChangeInterceptor = new LocaleChangeInterceptor();
//      localeChangeInterceptor.setParamName("language");
//      return localeChangeInterceptor;
//   }
//   @Override
//   public void addInterceptors(InterceptorRegistry registry) {
//      registry.addInterceptor(localeChangeInterceptor());
//   }
//   
//   @Bean
//   public LocaleResolver localeResolver(){
//          SessionLocaleResolver localeResolver = new SessionLocaleResolver();
//          localeResolver.setDefaultLocale(Locale.US);
//          return  localeResolver;
//      }
//   
//   @Bean
//   public MessageSource messageSource() {
//       ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
//       messageSource.setBasenames("language/i18n/messages");
//       messageSource.setDefaultEncoding("UTF-8");
//       return messageSource;
//   }
//   
//   
//}