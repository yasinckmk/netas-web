package com.netas.web.repo.jpa.entity;

import java.io.Serializable;
import java.util.Date;


public interface IModel extends Serializable {

	/**
	 * Model sinifin id si donulur
	 * 
	 * see AbstractBean#save()
	 * @param <I> is generic type
	 * @return id - Long
	 */
	<I extends Serializable> I getId();
	
	/**
	 * Modeli olusturan kullanici bilgisini doner
	 * @return create user
	 */
	String getCreateUser();
	
	/**
	 * Modeli olusturan kullanici bilgisini setler
	 * @param createUser is String value
	 */
	void setCreateUser(String createUser);
	
	/**
	 * Modelin olusturulma tarihini doner
	 * @return Date
	 */
	Date getCreateDate();
	
	/**
	 * Modelin olusturulma tarihini setler
	 * @param createDate is Date value
	 */
	void setCreateDate(Date createDate);
	
	/**
	 * Modeli duzenleyen kullanici bilgisini doner
	 * @return String
	 */
	String getUpdateUser();	
	
	/**
	 * Modeli duzenleyen kullanici bilgisini setler
	 * @param updateUser is String value
	 */
	void setUpdateUser(String updateUser);
	
	/**
	 * Modelin degistirilme tarihini setler
	 * @param updateDate is Date value
	 */
	void setUpdateDate(Date updateDate);
	
	/**
	 * Modeli olusturan kullanici bilgisi manuel olarak setlenmis mi
	 * @return boolean
	 */
	boolean isCreateUserManually();
	
	/**
	 * Modeli duzenleyen kullanici bilgisi manuel olarak setlenmis mi
	 * @return boolean
	 */
	boolean isUpdateUserManually();
	
}
