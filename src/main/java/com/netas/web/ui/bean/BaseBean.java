package com.netas.web.ui.bean;

import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;

public abstract class BaseBean {

	public static final String NETAS_LOGINNAME = "NETAS-LOGINNAME";

	private ResourceBundle bundle;

	public FacesContext getFacesContext() {
		return FacesContext.getCurrentInstance();
	}

	protected void addErrorMessage(String message) {
		addMessage(FacesMessage.SEVERITY_ERROR, message);
	}

	protected void addInfoMessage(String message) {
		addMessage(FacesMessage.SEVERITY_INFO, message);
	}

	protected void addExceptionMessage(Throwable throwable) {
		if (throwable.getMessage() != null && throwable.getMessage().length() > 0) {
			addErrorMessage(throwable.getMessage());
		} else if (throwable.getLocalizedMessage() != null && throwable.getLocalizedMessage().length() > 0) {
			addErrorMessage(throwable.getLocalizedMessage());
		} else {
			addErrorMessage(throwable.getClass().toString());
		}
	}

	protected void addInfoMessageFromResource(String key, String defaultValue, String... parameters) {
		addMessage(FacesMessage.SEVERITY_INFO, getMessage(key, defaultValue, parameters));
	}

	protected void addErrorMessageFromResource(String key, String defaultValue, String... parameters) {
		addMessage(FacesMessage.SEVERITY_ERROR, getMessage(key, defaultValue, parameters));
	}

	protected void addMessage(Severity severity, String message) {
		FacesMessage msg = new FacesMessage(severity, message, null);
		getFacesContext().addMessage(null, msg);
	}

	protected String getMessage(String key, String defaultValue, String... parameters) {

		String result = null;
		try {
			result = getBundle().getString(key);
			if (result != null) {
				return MessageFormat.format(result, parameters);
			} else {
				return result;
			}

		} catch (MissingResourceException e) {
			return defaultValue;
		}

	}

	protected ResourceBundle getBundle() {
		if (bundle == null) {
			bundle = getFacesContext().getApplication().getResourceBundle(getFacesContext(), "messages");
		}
		return bundle;
	}

}
