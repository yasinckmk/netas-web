package com.netas.web.ui.bean;

public enum CrudMode {

	VIEW,
	ADD,
	EDIT
	
}
