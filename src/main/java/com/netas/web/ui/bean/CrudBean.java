package com.netas.web.ui.bean;

import java.util.List;

import javax.annotation.PostConstruct;

import org.primefaces.event.RowEditEvent;
import org.slf4j.LoggerFactory;


import org.slf4j.Logger;

public abstract class CrudBean<T> extends BaseBean {

	protected T selected;

	protected List<T> filteredList;

	protected CrudMode crudMode = CrudMode.VIEW;

	protected String selectedFilterKey;

	protected static final Logger logger = LoggerFactory.getLogger(CrudBean.class);

	private int selectedIndex = -1;

	@PostConstruct
	public void init() {

		setCrudMode(CrudMode.VIEW);
	}

	public CrudBean() {
		selected = createNew();
	}

	public T getSelected() {
		return selected;
	}

	public void setSelected(T selected) {
		logger.info("Selected a emd");
		this.selected = selected;
		crudMode = CrudMode.EDIT;
		if (selected != null && crudMode == CrudMode.ADD) {
			crudMode = CrudMode.EDIT;
		}
		onSelect(selected);
	}

	public List<T> getFilteredList() {
		return filteredList;
	}

	public void setFilteredList(List<T> filteredList) {
		this.filteredList = filteredList;
	}

	public boolean isViewMode() {
		return CrudMode.VIEW == crudMode;
	}

	public void setViewMode(boolean isViewMode) {
		if (isViewMode) {
			setCrudMode(CrudMode.VIEW);
		} else {
			setCrudMode(CrudMode.EDIT);
		}
	}

	public void setEditMode(boolean isEditMode) {
		if (isEditMode) {
			setCrudMode(CrudMode.EDIT);
		} else {
			setCrudMode(CrudMode.ADD);
		}
	}

	public boolean isEditMode() {
		return CrudMode.EDIT == crudMode;
	}

	public boolean isAddMode() {
		return CrudMode.ADD == crudMode;
	}

	public CrudMode getCrudMode() {
		return crudMode;
	}

	public void setCrudMode(CrudMode crudMode) {

		this.crudMode = CrudMode.VIEW;

	}

	public String getSelectedFilterKey() {
		return selectedFilterKey;
	}

	public void setSelectedFilterKey(String selectedFilterKey) {
		this.selectedFilterKey = selectedFilterKey;
	}

	public void reset() {

		selected = createNew();

		setCrudMode(CrudMode.ADD);

		resetAction();

	}

	public void onRowEdit(RowEditEvent event) {
		T item = (T) event.getObject();
		if (!validate(item)) {
			addErrorMessage("validation error");
			return;
		}

	}

	public abstract T createNew();

	public abstract boolean validate(T t);

	public abstract void onSelect(T t);

	public abstract boolean prepare(T t);

	public abstract boolean addAction();

	public abstract boolean updateAction();

	public abstract boolean updateAction(T t);

	public abstract void resetAction();

	public abstract boolean deleteAction();
	
	
	public void onLoad()  {
		reset();
	}

	public int getSelectedIndex() {
		return selectedIndex;
	}

	public void setSelectedIndex(int selectedIndex) {
		this.selectedIndex = selectedIndex;
	}

	public void delete() {
		deleteAction();
	}

	public void save() {
		if (!validate(selected)) {
			return;
		}
		if (isEditMode()) {
			if (updateAction()) {
				resetAction();
				addInfoMessageFromResource("general.message.save.successful", "Save Successful");
			} else {
				addErrorMessageFromResource("general.message.save.fail", "Save Fail");
			}

		} else {

			if (addAction()) {
				resetAction();
				addInfoMessageFromResource("general.message.save.successful", "Save Successful");
			} else {
				addErrorMessageFromResource("general.message.save.fail", "Save Fail");

			}

		}

	}

}
