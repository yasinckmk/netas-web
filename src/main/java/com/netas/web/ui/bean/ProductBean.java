package com.netas.web.ui.bean;

import java.io.Serializable;

import javax.faces.bean.ViewScoped;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.netas.web.db.model.Product;
import com.netas.web.services.ProductServiceImpl;
@Component(value="productBean")
@ViewScoped
public class ProductBean extends CrudBean<Product> implements Serializable{

	private static final long serialVersionUID = 1L;
	@Autowired
	private ProductServiceImpl productService; 
	
	@Override
	public Product createNew() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean validate(Product t) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onSelect(Product t) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean prepare(Product t) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean addAction() {
          productService.saveProduct(selected);
		return false;
	}

	@Override
	public boolean updateAction() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean updateAction(Product t) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void resetAction() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean deleteAction() {
		// TODO Auto-generated method stub
		return false;
	}

}
