package com.netas.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

	@GetMapping("/")
	public String welcome() {
		return "welcome";
	}

	@GetMapping("/index")
	public String index() {
		return "index";
	}
	@GetMapping("/indext")
	public String indext() {
		return "indext";
	}
	
	@GetMapping("/product")
	public String prouctt() {
		return "product";
	}
}
