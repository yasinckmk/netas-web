package com.netas.web.db.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.validator.constraints.Length;

import com.netas.web.repo.jpa.entity.IModel;


@MappedSuperclass
public abstract class AbstractModel implements IModel {

	private static final long serialVersionUID = 1L;

	@Column(name = "CREATE_USER", length = 30)
	@Length(max = 30)
	private String createUser;

	@Column(name = "CREATE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate = new Date();

	@Column(name = "UPDATE_USER", length = 30)
	@Length(max = 30)
	private String updateUser;

	@Column(name = "UPDATE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	/**
	 * Olusturan user bilgisi manuel setlenmis mi
	 */
	@Transient
	private boolean createUserManually = false;

	/**
	 * Duzenleyen user bilgisi manuel setlenmis mi
	 */
	@Transient
	private boolean updateUserManually = false;

	public String getCreateUser() {
		return (createUser != null ? createUser : "N/A");
	}

	public void setCreateUser(String createUser) {
		createUserManually = true;
		this.createUser = createUser;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getUpdateUser() {
		return (updateUser != null ? updateUser : "N/A");
	}

	public void setUpdateUser(String updateUser) {
		updateUserManually = true;
		this.updateUser = updateUser;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public boolean isCreateUserManually() {
		return createUserManually;
	}

	public boolean isUpdateUserManually() {
		return updateUserManually;
	}

	@Transient
	@Override
	public int hashCode() {
		int hash = 0;
		hash += (getId() != null ? getId().hashCode() : super.hashCode());
		return hash;
	}

	@Transient
	@Override
	public boolean equals(Object obj) {
		boolean result = false;

		if (obj != null && obj instanceof IModel) {
			if (getClass().equals(obj.getClass())) {
				IModel model = (IModel) obj;
				result = (model.getId() != null && getId() != null && model.getId().equals(getId()));
			}
		}

		return result;
	}

}
