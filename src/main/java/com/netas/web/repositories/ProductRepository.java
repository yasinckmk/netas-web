package com.netas.web.repositories;

import org.springframework.data.repository.CrudRepository;

import com.netas.web.db.model.Product;

public interface ProductRepository extends CrudRepository<Product, Integer>{
}
